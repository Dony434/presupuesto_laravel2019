<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;

class ProductUpdateRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(Route $route)
    {
        $this->route = $route;

    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
       public function rules()
        {
            return [
               'name'        => 'required|string|min:3|max:50|regex:/^[a-z.\s]+$/i|unique:products,name,'.$this->route->hasParameters('products'),
               'description' => 'required|string|min:6|max:120',
            ];
        }

        public function messages()
        {
            return[
                'name.required'             => 'El campo producto es obligatorio',
                'name.unique:products,name' => 'El nombre del producto se repite',
                'name.min'                  => 'El minimo de caracteres son 3',
                'name.max'                  => 'Lo maximo de caracteres son (50)',
                'name.regex'                => 'En el campo producto solo se permite caracteres',
                'description.required'      => 'El campo descripción es obligatorio',
                'description.min'           => 'El minimo de caracteres son (6)',
                'description.max'           => 'Lo maximo de caracteres son (120)'
            ];
        }
}
