@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">

                    Usuario

                </div>
                <div class="card-body">

                    <p><strong>Nombre</strong>&nbsp &nbsp &nbsp {{ $user->name }}</p>
                    <p><strong>Email</strong>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp {{ $user->email }}</p>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection