@if (count($errors)>0)
  <div class='alert alert-danger' roler='alert'>
    <strong>Errores:</strong>
    <ul>

        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
        
    </ul>
  </div>
@endif